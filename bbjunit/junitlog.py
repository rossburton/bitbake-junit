#! /usr/bin/env python3

import logging
import logging.config
import junit_xml
import re

class Handler(logging.Handler):
    def __init__(self, filename):
        logging.Handler.__init__(self)
        print("JUnit logger writing to %s" % filename)
        self.filename = filename
        # recipe-task -> TestCase
        self.testcases = {}

    def emit(self, record):
        if hasattr(record, "recipe"):
            # If knotty has been patched to inject this data
            recipe = record.recipe
            message = record.origmsg
        else:
            # Plain bitbake
            if ":" not in record.msg:
                return
            recipe, message = record.msg.split(":", 1)
            message = message.strip()

        try:
            case = self.testcases[recipe]
        except KeyError:
            case = self.testcases[recipe] = junit_xml.TestCase(recipe, allow_multiple_subelements=True)
        
        if record.levelno == logging.ERROR:
            case.add_error_info(message)
        else:
            case.add_failure_info(message)

    def flush(self):
        if self.filename:
            print("Writing JUnit to %s" % self.filename)
            suite = junit_xml.TestSuite("BitBake", self.testcases.values())
            with open(self.filename, "wt") as f:
                f.write(junit_xml.TestSuite.to_xml_string([suite]))

    def close(self):
        if self.filename:
            self.flush()
            self.filename = None
        Handler.close(self)

if __name__ == "__main__":
    import os
    print(os.path.join(os.path.dirname(__file__), "junit.json"))
