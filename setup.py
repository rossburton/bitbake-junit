#! /usr/bin/env python3

import setuptools

setuptools.setup(
    name="bbjunit",
    version="4",
    author="Ross Burton",
    author_email="ross.burton@arm.com",
    python_requires='>=3.5',
    install_requires="junit-xml",
    packages=setuptools.find_packages(),
    package_data={
        "": ["*.json"]
    }
)
